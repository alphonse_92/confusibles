package main

import (
	"bufio"
	"encoding/hex"
	"fmt"
	"os"
	"strconv"
	"strings"
)

var input string

func scanner() []string {
	fmt.Print("Enter text: ")
	fmt.Scanln(&input)

	strArray := strings.Split(input, "")

	return strArray
}

func strtoHex(char string) string {
	src := []byte(char)
	hex := hex.EncodeToString(src)

	return hex
}

func hextoRune(char string) string {
	intSrt, _ := strconv.ParseInt(char, 16, 32)
	return string(intSrt)
}

func hextoInt64(hex string) int64 {
	hexInt, _ := strconv.ParseInt(hex, 16, 32)
	return hexInt
}

func readFile() {
	var array []string

	fileHandle, _ := os.Open("confusables.txt")
	defer fileHandle.Close()
	fileScanner := bufio.NewScanner(fileHandle)

	for fileScanner.Scan() {
		array = append(array, fileScanner.Text())
	}
	searchConfusables(array)
}

func searchConfusables(array []string) {
	strArray := scanner()
	alternatives := make([]string, len(strArray))
	similars := ""

	for i := 0; i < len(strArray); i++ {
		intInput := hextoInt64(strtoHex(strArray[i]))
		similars = ""

		for j := 0; j < len(array); j++ {
			fileLine := strings.Fields(array[j])

			if len(fileLine) > 0 {
				altValue := hextoRune(fileLine[0])
				intValue := hextoInt64(fileLine[1])

				if intValue == intInput {
					similars += altValue
				}
			}
		}
		alternatives[i] = similars
	}
	splitAlternatives(alternatives)

	/* for i := 0; i < len(array); i++ {
		var alternative []string
		fileLine := strings.Fields(array[i])

		if len(fileLine) > 0 {
			// Convert the first Column to Rune
			altValue := hextoRune(fileLine[0])
			// Convert the second Column to int64 For comparison
			intValue := hextoInt64(fileLine[1])

			for j := 0; j < len(strArray); j++ {
				// Convert the character to Hex first to convert to Int64 for comparison
				intInput := hextoInt64(strtoHex(strArray[j]))

				if intValue == intInput {
					alternative = append(alternative, altValue)
					fmt.Println(alternative)
				} else {
					alternative = append(alternative, strArray[j])
				}

			}
		}
		// fmt.Println(alternative)
	} */

}

func splitAlternatives(alternatives []string) {
	altSplitted := [][]string{}
	for i := 0; i < len(alternatives); i++ {
		altSplit := strings.Split(alternatives[i], "")
		altSplitted = append(altSplitted, altSplit)
	}

	getCombinations(altSplitted)
}

// Not my func but it works
func getCombinations(vids [][]string) [][]string {
	toRet := [][]string{}

	if len(vids) == 1 {
		for _, vid := range vids[0] {
			toRet = append(toRet, []string{vid})
		}
		return toRet
	}

	t := getCombinations(vids[1:])
	for _, vid := range vids[0] {
		for _, perm := range t {
			toRetAdd := append([]string{vid}, perm...)
			toRet = append(toRet, toRetAdd)
		}
	}

	printCombinations(toRet)

	return toRet
}

func printCombinations(combinations [][]string) {
	for _, internal := range combinations {
		fmt.Println(strings.Join(internal, " "))
	}
}

func main() {
	readFile()
}
